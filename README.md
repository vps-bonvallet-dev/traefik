# traefik

## Prepare the installation (no git, no docker, no docker-compose)

Connect to the server with ssh and execute the next line.
After that we have docker-compose installed ! 

```console
# Update the installed packages to the latest version
apt-update && apt-upgrade

# Install the dependencies necessary to add a new repository over HTTPS
apt install apt-transport-https ca-certificates curl software-properties-common gnupg2

# Import the repository’s GPG key
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -

# Add the Docker APT repository to your system’s software repository list
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"

# Install docker CE
apt update && apt install docker-ce -y

# download docker-compose binary
curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

# given write permission to binary
chmod +x /usr/local/bin/docker-compose

# install git 
apt install git -y
```

## Clone the code !
```console
mkdir -p /src
git clone https://gitlab.com/vps-bonvallet-dev/traefik.git /src/traefik
chmod 600 /src/traefik/acme.json
```

## launch traefik
```console
docker network create web
cd /src/traefik
docker-compose up -d
```
